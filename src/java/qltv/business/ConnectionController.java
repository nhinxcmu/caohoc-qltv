/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.business;

import java.io.IOException;
import vn.edu.vnu.jvntext.jvntextpro.JVnTextPro;
import vn.edu.vnu.jvntext.jvntextpro.conversion.CompositeUnicode2Unicode;
import vn.edu.vnu.jvntext.utils.InitializationException;


/**
 *
 * @author NGUYEN
 */
public final class ConnectionController {
    private static ConnectionController INSTANCE;
    public static CompositeUnicode2Unicode unicodeNormalizer;
    public static JVnTextPro vnTextPro;

    private ConnectionController() throws IOException, InitializationException { 
        init();
    }
    
    public static ConnectionController getInstance() throws IOException, InitializationException {
        if(INSTANCE == null) {
            INSTANCE = new ConnectionController();
        }
        return INSTANCE;
    }
    
    
    private static void init() throws IOException, InitializationException {
        vnTextPro = new JVnTextPro();
        vnTextPro.initSenTokenization();
        unicodeNormalizer = new CompositeUnicode2Unicode();

        vnTextPro.initSenSegmenter();
        // use this method instead if you have a custom model
        //        vnTextPro.initSenSegmenter(Paths.get("path", "to", "senSegModels"));

        vnTextPro.initSenTokenization();

        vnTextPro.initSegmenter();
        // use this method instead if you have a custom model
        //        vnTextPro.initSegmenter(Paths.get("path", "to", "segModels"));

        vnTextPro.initPosTagger();
        // use this method instead if you have a custom model
        //        vnTextPro.initPosTagger(Paths.get("path", "to", "posTagModels"));
    } 
    
    
    
}
