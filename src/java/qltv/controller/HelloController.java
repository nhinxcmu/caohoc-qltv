package qltv.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nguyen
 */
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.ModelMap;
//import qltv.model.Person;

@Controller
@RequestMapping("/hello")
public class HelloController {
   @RequestMapping(method = RequestMethod.GET)public String printHello(ModelMap model) {
      model.addAttribute("message", "Hello Spring MVC Framework!");
//      ObjectContainer db = null;
//        try
//        {
//            db = Db4oEmbedded.openFile("F:/persons.data");
// 
//            Person brian = new Person("Brian", "Goetz", 39);
//            Person jason = new Person("Jason", "Hunter", 35);
//            Person brians = new Person("Brian", "Sletten", 38);
//            Person david = new Person("David", "Geary", 55);
//            Person glenn = new Person("Glenn", "Vanderberg", 40);
//            Person neal = new Person("Neal", "Ford", 39);
//             
//            db.store(brian);
//            db.store(jason);
//            db.store(brians);
//            db.store(david);
//            db.store(glenn);
//            db.store(neal);
// 
//            db.commit();
//             
//            // Find all the Brians
//            System.out.println("Stored " + brian);
//        }
//        finally
//        {
//            if (db != null)
//                db.close();
//        }
      return "index";
   }
}
