/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import javax.servlet.http.HttpServletRequest;
import qltv.business.Config;


import java.io.IOException;
import java.io.InputStream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import vn.edu.vnu.jvntext.jvntextpro.JVnTextPro;

import vn.edu.vnu.jvntext.jvntextpro.conversion.CompositeUnicode2Unicode;
import vn.edu.vnu.jvntext.utils.InitializationException;


import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.MongoException;
import java.net.UnknownHostException;
import org.bson.types.ObjectId;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import qltv.business.ConnectionController;
import org.json.JSONObject;

/**
 *
 * @author nguye
 */
@Controller
@RequestMapping("/")
public class HomeController {

    
    
    @RequestMapping("")
    public String index(ModelMap model,String ten, HttpServletRequest request) throws IOException, InitializationException {
        try{
                

        
        } catch (MongoException e) {
            e.printStackTrace();
        }
        
        
        
        return "index";
    }
    
    @RequestMapping("/invertedindex")
    public String invertedindex(ModelMap model,String ten, HttpServletRequest request) throws IOException, InitializationException {
        return "invertedindex";
    }
    
    
    @RequestMapping(value = "invertedindex", method = RequestMethod.POST)
    public String invertedindex(HttpServletRequest req, HttpServletResponse response) 
            throws ServletException, IOException, Exception{
        MongoClient mongo = new MongoClient("localhost", 27017);
        DB db = mongo.getDB("vnptsearch");
       
        
        try {
            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(req);

            for (FileItem item : items) {
                if (item.isFormField()) {
                    // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
                    String fieldName = item.getFieldName();
                    String fieldValue = item.getString();
                    // ... (do your job here)
                } else {
                    // Process form file field (input type="file").
                    String fieldName = item.getFieldName();
                    
                        String fileName = FilenameUtils.getName(item.getName());
                    if(!fileName.equals("")) {
                        String filePath = Config.class.getResource("").getPath()+ "../../../upload/" + File.separator + fileName;
                        File storeFile = new File(filePath);

                        item.write(storeFile);
                        DBCollection doc = db.getCollection("document");
                        BasicDBObject docElement = new BasicDBObject();
                        
                        DBCollection invertedIndex = db.getCollection("inverted_index");
                        
                        
                        try {
                                BufferedReader reader = new BufferedReader(new InputStreamReader(
                                                new FileInputStream(storeFile), "UTF-8"));

                                String line, data = "";
                                while((line = reader.readLine()) != null){
                                        data += line + "\n";				
                                }
                                reader.close();
                                docElement.put("name", fileName);
                                docElement.put("path", filePath);
                                docElement.put("content", data);
                                doc.insert(docElement);
                                ObjectId id = (ObjectId)docElement.get("_id");
                                data = data.toLowerCase();
                                String converted = ConnectionController.getInstance().unicodeNormalizer.convert(data);    
                                String annotated = ConnectionController.getInstance().vnTextPro.process(converted);
                                
                                String[] words=annotated.split("\\s");
                                List<String> exit = new ArrayList<>();
                                long frequency = 1;
                                long max = 0;
                                long findmax = 0;
                                for (int i = 0; i < words.length; i++) {
                                    String word = words[i];
                                    findmax = 0;
                                    if(exit.indexOf(word) == -1) {
                                        for (int j = i+1; j < words.length; j++) {
                                            
                                            if(word.equals(words[j])) {
                                                
                                                findmax++;
                                                if(findmax > max) {
                                                    max = findmax;
                                                }
                                            }
                                        }
                                        

                                    }
                                }
                                exit = new ArrayList<>();
                                long allDoc = doc.count();
                                for (int i = 0; i < words.length; i++) {
                                    String word = words[i];
                                    frequency = 1;
                                    List<Integer> position = new ArrayList<>();
                                    if(exit.indexOf(word) == -1) {
                                        for (int j = i+1; j < words.length; j++) {
                                            
                                            if(word.equals(words[j])) {
                                                position.add(j);
                                                frequency++;
                                            }
                                        }
                                        exit.add(word);
                                        BasicDBObject searchQuery 
                                            = new BasicDBObject().append("term", word);
                                        DBCursor cursor = invertedIndex.find(searchQuery);
                                        if(cursor.hasNext()) {
                                            DBObject obj = cursor.next();
                                            List<BasicDBObject> array = (ArrayList<BasicDBObject>)obj.get("docs");
                                            BasicDBObject query = new BasicDBObject();
                                            query.put("_id", obj.get("_id"));
                                            BasicDBObject _doc = new BasicDBObject();
                                            _doc.put("docID", id);
                                            _doc.put("frequency", (double)frequency/max);
                                            _doc.put("positions", position);
                                            array.add(_doc);
                                            BasicDBObject _docs = new BasicDBObject();
                                            _docs.put("docs", array);
                                            _docs.put("df", array.size());
                                            _docs.put("idf", (double)(1+ Math.log10(allDoc/array.size())));
                                            BasicDBObject updateObj = new BasicDBObject();
                                            updateObj.put("$set", _docs);
                                            
                                            invertedIndex.update(query, updateObj);
                                        } else {
                                            BasicDBObject _index = new BasicDBObject();
                                            _index.put("term", word);
                                            List<BasicDBObject> array = new ArrayList<BasicDBObject>();
                                            BasicDBObject _doc = new BasicDBObject();
                                            _doc.put("docID", id);
                                            _doc.put("frequency", (double)frequency/max);
                                            _doc.put("positions", position);
                                            array.add(_doc);
                                            _index.put("docs", array);
                                            _index.put("df", array.size());
                                            _index.put("idf", (double)(1+ Math.log10(allDoc/array.size())));
                                            invertedIndex.insert(_index);
                                        }

                                    }
                                }
                        }
                        catch (Exception e){
                                System.out.println(e.getMessage());
                                e.printStackTrace();
                                
                        }
                        
                        
                        
                    }

                }
            }
        } catch (FileUploadException e) {
            //redir.addFlashAttribute("message", "Lỗi");
            //redir.addFlashAttribute("messageClass", "error");
        }
        
        return "redirect:invertedindex";
    }
    
    @RequestMapping(value = "consine", method = RequestMethod.POST)
    public String consine(ModelMap model,HttpServletRequest req, HttpServletResponse response) 
            throws ServletException, IOException, Exception{
        MongoClient mongo = new MongoClient("localhost", 27017);
        DB db = mongo.getDB("vnptsearch");
       JSONObject cosineDoc = new JSONObject();
        
        try {
            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(req);

            for (FileItem item : items) {
                if (item.isFormField()) {
                    // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
                    String fieldName = item.getFieldName();
                    String fieldValue = item.getString().toLowerCase();
                    // ... (do your job here)
                    String converted = ConnectionController.getInstance().unicodeNormalizer.convert(fieldValue);    
                    String annotated = ConnectionController.getInstance().vnTextPro.process(converted);
                    String[] words=annotated.split("\\s");
                    DBCollection invertedIndex = db.getCollection("inverted_index");
                    DBCollection doc = db.getCollection("document");
                    long allDoc = doc.count();
                    DBCursor cursor = doc.find();
                    List<String> exit = new ArrayList<>();
                    JSONObject tfidfTerm = new JSONObject();
                    
                    long max = 1;
                    long findmax = 1;
                    for (int i = 0; i < words.length; i++) {
                        String word = words[i];
                        findmax = 1;
                        if(exit.indexOf(word) == -1) {
                            for (int j = i+1; j < words.length; j++) {

                                if(word.equals(words[j])) {

                                    findmax++;
                                    if(findmax > max) {
                                        max = findmax;
                                    }
                                }
                            }
                            exit.add(word);


                        }
                    }
                    exit = new ArrayList<>();
                    long frequency = 1;
                    for (int i = 0; i < words.length; i++) {
                        String word = words[i];
                        frequency = 1;
                        if(exit.indexOf(word) == -1) {
                            for (int j = i+1; j < words.length; j++) {
                                if(word.equals(words[j])) {
                                    frequency++;
                                }
                            }
                            
                            BasicDBObject searchQuery =
                                    new BasicDBObject().append("term", words[i]);
                            DBCursor cursorIndex = invertedIndex.find(searchQuery);
                            double tf = frequency/max;
                            double idf = 0;
                            if(cursorIndex.hasNext()) {
                                DBObject _term = cursorIndex.next();
                                double _log = allDoc/(int)_term.get("df");
                                idf = 1+Math.log10(_log);
                            } 
                            tfidfTerm.put(word, tf*idf);
                            exit.add(word);
                        }
                            
                    }
                    
                    while (cursor.hasNext()) {
                        DBObject objDoc = cursor.next();
                        ObjectId docObjectID = (ObjectId)objDoc.get("_id");
                        double _sumtfidfsquareTerm = 0;
                        double _sumtfidfsquareDoc = 0;
                        double _sumtfidf = 0;
                        for (int i = 0; i < exit.size(); i++) {
                            BasicDBObject searchQuery =
                                    new BasicDBObject().append("term", exit.get(i)).append("docs.docID", docObjectID);
                            DBCursor cursorIndex = invertedIndex.find(searchQuery);
                            double idf = 0;
                            double tf = 0;
                            if(cursorIndex.hasNext()) {
                                DBObject _term = cursorIndex.next();
                                
                                List<BasicDBObject> array = (ArrayList<BasicDBObject>)_term.get("docs");
                                idf = (double)(1+ Math.log10(allDoc/array.size()));
                                for (int j = 0; j < array.size(); j++) {
                                    String id = array.get(j).get("docID").toString();
                                    if(id.equals(docObjectID.toString())){
                                        tf = (double)array.get(j).get("frequency");
                                    }
                                }
                                
                            }
                            _sumtfidf += tfidfTerm.getDouble(exit.get(i))*idf*tf;
                            _sumtfidfsquareTerm += Math.pow(tfidfTerm.getDouble(exit.get(i)),2);
                            _sumtfidfsquareDoc += Math.pow(idf*tf,2);
                        }
                        double _temp =(Math.sqrt(_sumtfidfsquareTerm)*Math.sqrt(_sumtfidfsquareDoc));
                        if(_temp == 0) {
                            cosineDoc.put(objDoc.get("_id").toString(), 0);
                        } else {
                            cosineDoc.put(objDoc.get("_id").toString(), _sumtfidf/_temp);
                        }
                        
                    }
                    
                    
                } 

            }
        } catch (FileUploadException e) {
            //redir.addFlashAttribute("message", "Lỗi");
            //redir.addFlashAttribute("messageClass", "error");
        }
        model.put("json", cosineDoc);
        return "json";
    }
}
