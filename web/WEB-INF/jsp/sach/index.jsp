<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý thư viện</title>
    </head>

    <body>
        <!--Main Navigation-->
        <%@include file="../menu.jsp"%>
        <!--Main Navigation-->

    <main>
        
        <div class="container-fluid">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb cyan lighten-4">
                    <li class="breadcrumb-item"><a class="black-text" href="${pageContext.request.contextPath}">Trang chủ</a></li>
                    <li class="breadcrumb-item active">Danh sách Sách</li>
                </ol>
            </nav>
            <form class="row" id="form">
                <div class="col-md-10">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 ">
                                <div class="md-form form-sm">
                                    <input type="text" id="txtMaSach" class="form-control form-control-sm" name="maSach">
                                    <label for="txtMaSach">Mã sách</label></div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form form-sm">
                                    <input type="text" id="txtTenSach" class="form-control form-control-sm" name="tenSach">
                                    <label for="txtTenSach">Tên sách</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form form-sm ">
                                    <label class="active" for="txtTuNgay">Từ ngày</label>
                                    <input type="date" id="txtTuNgay" class="form-control form-control-sm" name="tuNgay">
                                </div>

                            </div>
                            <div class="col-md-6 ">
                                <div class="md-form form-sm ">
                                    <label class="active" for="txtDenNgay">Đến ngày</label>
                                    <input type="date" id="txtDenNgay" class="form-control form-control-sm" name="denNgay">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form form-sm">
                                    <input type="text" id="txtTomTat" class="form-control form-control-sm" name="tomTat">
                                    <label for="txtTomTat">Tóm tắt</label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="text-align: center">
                    <button type="submit" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-search" aria-hidden="true"></i> Tìm kiếm</button>
                </div>
            </form>
            <div class="divider-new">Danh sách tựa sách</div>
            <div class="row">
                <div class="col-auto">
                    <a class="btn btn-success btn-sm" href="${pageContext.request.contextPath}/Sach/ThemSach"><i class="fa fa-plus" aria-hidden="true"></i> Thêm sách </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table id="m_dssach" class="table table-sm table-bordered table-striped" style="text-align: center">
                        <thead>
                            <tr>
                                <th>
                                    Mã sách
                                </th>
                                <th>
                                    Tên sách
                                </th>
                                <th>
                                    Ngày phát hành
                                </th>
                                <th>
                                    Tóm tắt
                                </th>
                                <th>Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${sachs}" var="sach">
                                <tr>
                                    <td>
                                        ${sach.getMaSach()}
                                    </td>
                                    <td>
                                        ${sach.getTenSach()}
                                    </td>
                                    <td>
                                        ${sach.getNgayPhatHanh().toString()}
                                    </td>
                                    <td>
                                        ${sach.getTomTat()}
                                    </td>
                                    <td style="text-align: center;letter-spacing: 5px;">
                                        <a style="color:green" href="SuaSach?maSach=${sach.getMaSach()}"><i class="fa fa-pencil"></i></a>
                                        <a style="color:red" onclick="return confirm('Bạn thực sự muốn xóa?');" href="XoaSach?maSach=${sach.getMaSach()}"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>

                            </c:forEach>

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </main>
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/site.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/mdb.css" />"  rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css" />"  rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <script src="<c:url value="/resources/js/jquery.min.js" />" src=""></script>
    <script src="<c:url value="/resources/js/bootstrap.js" />" src=""></script>
    <script src="<c:url value="/resources/js/mdb.js" />"></script>
    <link href="<c:url value="/resources/jqgrid/css/ui.jqgrid.css" />" rel="stylesheet"/>           
    <script src="<c:url value="/resources/jqgrid/js/i18n/grid.locale-en.js" />"></script>            
    <script src="<c:url value="/resources/jqgrid/js/jquery.jqGrid.src.js" />"></script>
    <%@include file="../footer.jsp"%>
    <script>
            $(function(){
                $('#m_dssach').DataTable();
            })
    </script> 
</body>
</html>
