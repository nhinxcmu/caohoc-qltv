<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý thư viện</title>
    </head>

    <body>
        <!--Main Navigation-->
        <%@include file="../menu.jsp"%>
        <!--Main Navigation-->

    <main>
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb cyan lighten-4"> 
                    <li class="breadcrumb-item"><a class="black-text" href="${pageContext.request.contextPath}">Trang chủ</a></li>
                    <li class="breadcrumb-item"><a class="black-text" href="${pageContext.request.contextPath}/Sach">Sách</a></li>
                    <li class="breadcrumb-item active">Thêm Sách</li>
                </ol>
            </nav>
            <div class="row">
                <div class="col">
                    <form method="post">
                        <div class="form-group row">
                            <label class="col-md-2">Mã Sách </label>

                            <div class="col-md-10">
                                <input value="${sach.getMaSach()}" class="form-control" type="text" name="maSach"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2">Tên Sách</label>
                            <div class="col-md-10">
                                <input value="${sach.getTenSach()}" class="form-control" type="text" name="tenSach"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2">Ngày phát hành</label>
                            <div class="col-md-10">
                                
                                <input value="${sach.getNgayPhatHanhForHtml()}" class="form-control" type="date" name="ngayPhatHanh"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2">Tóm tắt</label>
                            <div class="col-md-10">
                                <textarea class="form-control"  name="tomTat">${sach.getTomTat()}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2">Tác giả</label>
                            <div class="col-md-10">
                                <select class="chosen-select form-control" multiple name="tacGias" placeholder="Chọn tác giả">
                                     <c:forEach items="${tacGias}" var="tacGia">
                                         <option ${sach.getTacGias().contains(tacGia)?"selected":""} value="${tacGia.getMaTacGia()}">${tacGia.getHoTen()}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <div class="col-auto">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Thêm</button>
                            </div>
                            <div class="col-auto">
                                <button type="reset" class="btn btn-secondary waves-effect waves-light">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col">
                    <span class="${messageClass}">${message}</span>
                </div>
            </div>
        </div>
    </main>

    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/site.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/mdb.css" />"  rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css" />"  rel="stylesheet">
    <link href="<c:url value="/resources/css/chosen.css" />" rel="stylesheet"/>      
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <script src="<c:url value="/resources/js/jquery.min.js" />" src=""></script>
    <script src="<c:url value="/resources/js/bootstrap.js" />" src=""></script>
    <script src="<c:url value="/resources/js/mdb.js" />"></script>
    <link href="<c:url value="/resources/jqgrid/css/ui.jqgrid.css" />" rel="stylesheet"/>      
    <script src="<c:url value="/resources/jqgrid/js/i18n/grid.locale-en.js" />"></script>            
    <script src="<c:url value="/resources/jqgrid/js/jquery.jqGrid.src.js" />"></script>            
    <script src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
    <script>
        $.ajax()
        $(".chosen-select").chosen();
    </script>
    <%@include file="../footer.jsp"%>
</body>
</html>
