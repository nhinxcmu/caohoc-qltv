<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý thư viện</title>
    </head>

    <body>
        <!--Main Navigation-->
    <%@include file="../menu.jsp"%>
    <!--Main Navigation-->

    <main>
        <div class="container-fluid">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb cyan lighten-4">
                  <li class="breadcrumb-item"><a class="black-text" href="${pageContext.request.contextPath}">Trang chủ</a></li>
                  <li class="breadcrumb-item active">Danh sách thành viên</li>
                </ol>
            </nav>
            <form class="row" id="form">
                <div class="col-md-10">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4 ">
                                <div class="md-form form-sm">
                                    <input type="text" id="txtmaThanhVien" class="form-control form-control-sm" name="maThanhVien">
                                    <label for="txtmaThanhVien">Mã Thành Viên</label></div>
                            </div>
                            <div class="col-md-4">
                                <div class="md-form form-sm">
                                    <input type="text" id="txthoTen" class="form-control form-control-sm" name="hoTen">
                                    <label for="txthoTen">Tên Thành Viên</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="md-form form-sm">
                                    <input type="text" id="txtcmnd" class="form-control form-control-sm" name="cmnd">
                                    <label for="txtcmnd">CMND</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="text-align: center">
                    <button type="submit" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-search" aria-hidden="true"></i> Tìm kiếm</button>
                </div>
            </form>
            <div class="divider-new">Danh sách thành viên</div>
            <div class="row">
                <div class="col-auto">
                    <a class="btn btn-success btn-sm" href="" data-toggle="modal" data-target="#modalRegisterForm"><i class="fa fa-plus" aria-hidden="true"></i> Thêm thành viên </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table id="table_thanhvien" class="table table-sm table-bordered table-striped" style="text-align: center">
                        <thead>
                            <tr>
                                <th>
                                    Mã thành viên 
                                </th>
                                <th>
                                    Tên thành viên
                                </th>
                                <th>
                                    Năm sinh
                                </th>
                                <th>
                                    CMND 
                                </th>
                                <th>Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${thanhviens}" var="thanhvien">
                                <tr>
                                    <td>
                                        ${thanhvien.getMaThanhVien()}
                                    </td>
                                    <td>
                                        ${thanhvien.getHoTen()}
                                    </td>
                                    <td>
                                        ${formatter.format(thanhvien.getNgaySinh())}
                                    </td>
                                    <td>
                                        ${thanhvien.getcMND()}
                                    </td>
                                    <td style="text-align: center;letter-spacing: 5px;">
                                        <a style="color:green" href="" data-toggle="modal" data-target="#modalRegisterForm" class="edit" data-id="${thanhvien.getMaThanhVien()}"><i class="fa fa-pencil"></i></a>
                                        <a style="color:red" data-id="${thanhvien.getMaThanhVien()}" class="delete"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>

                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header text-center">
                  <h4 class="modal-title w-100 font-weight-bold">Thông tin thành viên</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body mx-3">
                  <div class="md-form mb-5">
                    <i class="fa fa-user prefix grey-text"></i>
                    <input type="text" id="m_mathanhvien" class="form-control">
                    <label data-error="wrong" class="m_active" data-success="right" for="m_mathanhvien">Mã thành viên</label>
                  </div>
                  <div class="md-form mb-5">
                    <i class="fa fa-address-book prefix grey-text"></i>
                    <input type="email" id="m_hoten" class="form-control">
                    <label data-error="wrong" class="m_active" data-success="right" for="m_hoten">Họ tên</label>
                  </div>

                  <div class="md-form mb-4">
                    <i class="fa fa-address-card prefix grey-text"></i>
                    <input type="text" id="m_cmnd" class="form-control">
                    <label data-error="wrong" class="m_active" data-success="right" for="m_cmnd">CMND</label>
                  </div>
                   <div class="md-form mb-4">
                    <i class="fa fa-calendar prefix grey-text"></i>
                    <input type="date" id="m_ngaysinh" class="form-control" value="01/01/1990">
                    <label data-error="wrong" class="active" data-success="right" for="m_ngaysinh">Ngày sinh</label>
                  </div> 
                </div>
                <div class="modal-footer d-flex justify-content-center">
                  <button id="luuthongtin" class="btn btn-deep-orange">Lưu</button>
                </div>
              </div>
            </div>
        </div>
        <div class="modal fade top" id="frameModalBottom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-frame modal-top" role="document">


              <div class="modal-content">
                <div class="modal-body">
                  <div class="row d-flex justify-content-center align-items-center">

                    <p class="pt-3 pr-2" id="content_popup"></p>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                  </div>
                </div>
              </div>
            </div>
        </div>           
    </main>
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/site.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/mdb.css" />"  rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css" />"  rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <script src="<c:url value="/resources/js/jquery.min.js" />" src=""></script>
    <script src="<c:url value="/resources/js/bootstrap.js" />" src=""></script>
    <script src="<c:url value="/resources/js/mdb.js" />"></script>
    <link href="<c:url value="/resources/js/jqgrid/css/ui.jqgrid.css" />" rel="stylesheet"/>           
    <script src="<c:url value="/resources/js/jqgrid/js/i18n/grid.locale-en.js" />"></script>            
    <script src="<c:url value="/resources/js/jqgrid/js/jquery.jqGrid.src.js" />"></script>
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>
 
    <%@include file="../footer.jsp"%>
    <script>
        function splitDate(string) {
            var t = string.split("-");
            return t[2]+"/"+t[1]+"/"+t[0];
        }
        $(function() {
            var table = $('#table_thanhvien').DataTable();
            var row;
            var rowData;
            $("#luuthongtin").click(function(){
                $.ajax({
                    url: "ThemThanhVien",
                    type: "POST",
                    data: {
                        maThanhvien: $("#m_mathanhvien").val(),
                        hoTen: $("#m_hoten").val(),
                        ngaySinh: $("#m_ngaysinh").val(),
                        cmnd: $("#m_cmnd").val()
                    },
                    success: function(response) {
                        $("#modalRegisterForm").modal("hide");
                        $("#content_popup").html(response);
                        $("#frameModalBottom").modal('show');
                        if (row != null) {
                            table.row(rowData).data([row[0],$("#m_hoten").val(), splitDate($("#m_ngaysinh").val()),$("#m_cmnd").val()
                            ,row[4]]).draw();
                        } else {
                            table.row.add( [
                                $("#m_mathanhvien").val(),
                                $("#m_hoten").val(),
                                splitDate($("#m_ngaysinh").val()),
                                $("#m_cmnd").val(),
                                '<a style="color:green" href="" data-toggle="modal" data-target="#modalRegisterForm" class="edit" data-id="'+ $("#m_mathanhvien").val()+'"><i class="fa fa-pencil"></i></a><a style="color:red" data-id="'+ $("#m_mathanhvien").val()+'" class="delete"><i class="fa fa-trash"></i></a>'
                            ] ).draw( false );
                        }
                        
                    }
                })
                
            })
            $("body").on("click",".edit", function() {
                var mathanhvien = $(this).attr("data-id");
                rowData  = $(this).parent().parent();
                row = table.row($(this).parent().parent()).data();
                
                $.ajax({url: "${pageContext.request.contextPath}/ThanhVien/getThanhVien?maThanhVien="+mathanhvien,
                        type: "GET",
                        dataType: "json",
                        success: function(response) {
                            $("#m_mathanhvien").prop("readonly", true);
                            console.log("reponse", response.ngaySinh);
                            $("#m_mathanhvien").val(response.maThanhVien);
                            $("#m_hoten").val(response.hoTen);
                            document.getElementById("m_ngaysinh").valueAsDate = new Date(response.ngaySinh)
                            $("#m_cmnd").val(response.cmnd);
                            $(".m_active").addClass("active");
                        }
                })
            })
            $("body").on("click", ".delete", function() {
                var r = confirm("Bạn có chắc sẽ xoá dữ liệu này?");
                if (r == true) {
                    var mathanhvien = $(this).attr("data-id");
                    rowData  = $(this).parent().parent();
                    row = table.row($(this).parent().parent()).data();

                    $.ajax({url: "${pageContext.request.contextPath}/ThanhVien/XoaThanhVien?maThanhvien="+mathanhvien,
                            type: "POST",
                            dataType: "json",
                            success: function(response) {
                                if(response == '0') {
                                    $("#content_popup").html("Xoá thành công.");
                                    table.row(rowData).remove().draw();
                                } else {
                                    $("#content_popup").html("Bạn không thể xoá do đã phát sinh dữ liệu.");
                                }
                                $("#modalRegisterForm").modal("hide");
                                $("#frameModalBottom").modal('show');
                            }
                    })
                } 
                
            })
            
            $('#modalRegisterForm').on('hidden.bs.modal', function () {
                $("#m_mathanhvien").prop("readonly", false);
                row = null;
             })

        })
    </script> 
</body>
</html>
