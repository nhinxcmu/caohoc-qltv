<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý thư viện</title>
    </head>

    <body>
        <!--Main Navigation-->
    <%@include file="../menu.jsp"%>
    <!--Main Navigation-->

    <main>
        
        <div class="container-fluid">
            <nav aria-label="breadcrumb ">
                <ol class="breadcrumb cyan lighten-4">
                  <li class="breadcrumb-item"><a class="black-text" href="${pageContext.request.contextPath}">Trang chủ</a></li>
                  <li class="breadcrumb-item active">Danh sách bản sao sách</li>
                </ol>
            </nav>
            <form class="row" id="form">
                <div class="col-md-10">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 ">
                                <div class="md-form form-sm">
                                    <input type="text" id="txtMaSach" class="form-control form-control-sm" name="maSach">
                                    <label for="txtMaSach">Mã sách</label></div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="md-form form-sm">
                                    <input type="text" id="txtmaBanSao" class="form-control form-control-sm" name="maBanSao">
                                    <label for="txtmaBanSao">Mã bản sao</label></div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form form-sm">
                                    <input type="text" id="txtTenSach" class="form-control form-control-sm" name="tenSach">
                                    <label for="txtTenSach">Tên sách</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form form-sm ">
                                    <label class="active" for="txtTuNgay">Ngày phát hành</label>
                                    <input type="date" id="txtTuNgay" class="form-control form-control-sm" name="tuNgay">
                                </div>

                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="text-align: center">
                    <button type="submit" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-search" aria-hidden="true"></i> Tìm kiếm</button>
                </div>
            </form>
            <div class="divider-new">Danh sách tựa sách</div>
            <div class="row">
                <div class="col-auto">
                    <a class="btn btn-success btn-sm" href="" data-toggle="modal" data-target="#modalRegisterForm"><i class="fa fa-plus" aria-hidden="true"></i> Thêm bản sao</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table id="m_bansaosach" class="table table-sm table-bordered table-striped" style="text-align: center">
                        <thead>
                            <tr>
                                <th>
                                    Mã bản sao
                                </th>
                                <th>
                                    Mã sách
                                </th>
                                <th>
                                    Tên sách
                                </th>
                                <th>
                                    Ngày phát hành
                                </th>
                                <th>
                                    Tóm tắt
                                </th>
                                <th>
                                    Ngày xuất bản
                                </th>
                                <th>
                                    Giá tiền
                                </th>
                                <th>Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${sachs}" var="sach">
                                <tr>
                                    <td>
                                        ${sach.getmaBanSao()}
                                    </td>
                                    <td>
                                        ${sach.getSach().getMaSach()}
                                    </td>
                                    <td>
                                        ${sach.sach.getTenSach()}
                                    </td>
                                    <td>
                                        ${formatter.format(sach.sach.getNgayPhatHanh())}
                                    </td>
                                    <td>
                                        ${sach.sach.getTomTat()}
                                    </td>
                                    <td>
                                        ${formatter.format(sach.getngayXuatBan())}
                                    </td>
                                    <td>
                                        ${sach.getgiaTien()}
                                    </td>
                                    <td style="text-align: center;letter-spacing: 5px;">
                                        <a style="color:green" href="" data-toggle="modal" data-target="#modalRegisterForm" class="edit" data-id="${sach.getmaBanSao()}"><i class="fa fa-pencil"></i></a>
                                        <a style="color:red" data-id="${sach.getmaBanSao()}" class="delete"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>

                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header text-center">
                  <h4 class="modal-title w-100 font-weight-bold">Thông tin bản sao</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body mx-8">
                    <div class="form-group row">
                            <label class="col-md-1">Sách</label>
                            <div class="col-md-11">
                                <select class="form-control" id="m_idsach">
                                    <c:forEach items="${bangocs}" var="bangoc">
                                        <option value="${bangoc.getMaSach()}">${bangoc.getTenSach()}</option>
                                    </c:forEach>
                                </select>
                            </div>
                    </div>
                  <div class="md-form mb-5">
                    <i class="fa fa-address-book prefix grey-text"></i>
                    <input type="email" id="m_mabansao" class="form-control">
                    <label data-error="wrong" class="m_active" data-success="right" for="m_mabansao">Mã bản sao</label>
                  </div>
                   <div class="md-form mb-5">
                    <i class="fa fa-address-book prefix grey-text"></i>
                    <input type="number" id="m_trangthai" class="form-control">
                    <label data-error="wrong" class="m_active" data-success="right" for="m_trangthai">Trạng thái</label>
                  </div>     
                  <div class="md-form mb-4">
                    <i class="fa fa-address-card prefix grey-text"></i>
                    <input type="number" id="m_giatien" class="form-control">
                    <label data-error="wrong" class="m_active" data-success="right" for="m_giatien">Giá tiền</label>
                  </div>
                   <div class="md-form mb-4">
                    <i class="fa fa-calendar prefix grey-text"></i>
                    <input type="date" id="m_ngayxuatban" class="form-control" value="">
                    <label data-error="wrong" class="active" data-success="right" for="m_ngayxuatban">Ngày xuất bản</label>
                  </div> 
                </div>
                <div class="modal-footer d-flex justify-content-center">
                  <button id="luuthongtin" class="btn btn-deep-orange">Lưu</button>
                </div>
              </div>
            </div>
        </div>
        <div class="modal fade top" id="frameModalBottom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-frame modal-top" role="document">


              <div class="modal-content">
                <div class="modal-body">
                  <div class="row d-flex justify-content-center align-items-center">

                    <p class="pt-3 pr-2" id="content_popup"></p>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                  </div>
                </div>
              </div>
            </div>
        </div>          
    </main>
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/site.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/mdb.css" />"  rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css" />"  rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <script src="<c:url value="/resources/js/jquery.min.js" />" src=""></script>
    <script src="<c:url value="/resources/js/bootstrap.js" />" src=""></script>
    <script src="<c:url value="/resources/js/mdb.js" />"></script>

    <%@include file="../footer.jsp"%>
    <script>
        function splitDate(string) {
            var t = string.split("-");
            return t[2]+"/"+t[1]+"/"+t[0];
        }
        $(function() {
            var table = $('#m_bansaosach').DataTable();
            var row;
            var rowData;
            $("#luuthongtin").click(function(){
                var masach = $("#m_idsach").val();
                if (masach.length == 0 ) {
                    alert("Vui lòng chọn sách mượn ");
                    return false;
                } 
                $.ajax({
                    url: "${pageContext.request.contextPath}/BanSaoSach/ThemBanSao",
                    type: "POST",
                    data: {
                        maSach: $("#m_idsach").val(),
                        maBansao: $("#m_mabansao").val(),
                        trangthai: $("#m_trangthai").val(),
                        giaTien: $("#m_giatien").val(),
                        ngayXuatban: $("#m_ngayxuatban").val()
                    },
                    success: function(response) {
                        $("#modalRegisterForm").modal("hide");
                        $("#content_popup").html(response);
                        $("#frameModalBottom").modal('show');
                        $.ajax({url: "${pageContext.request.contextPath}/BanSaoSach/getBanSaoSach?maBanSao="+$("#m_mabansao").val(),
                            type: "GET",
                            dataType: "json",
                            success: function(response) {
                                if (row != null) {
                                    table.row(rowData).data([row[0],$("#m_idsach").val(), 
                                        response.tenSach,response.ngayPhatHanh,response.tomTat,
                                        
                                        splitDate($("#m_ngayxuatban").val()),$("#m_giatien").val()
                                    ,row[7]]).draw();
                                } else {
                                    table.row.add( [
                                        $("#m_mabansao").val(),
                                        $("#m_idsach").val(), 
                                        response.tenSach,response.ngayPhatHanh,response.tomTat,
                                        
                                        splitDate($("#m_ngayxuatban").val()),$("#m_giatien").val(),
                                        '<a style="color:green" href="" data-toggle="modal" data-target="#modalRegisterForm" class="edit" data-id="'+ $("#m_mabansao").val()+'"><i class="fa fa-pencil"></i></a><a style="color:red" data-id="'+ $("#m_mabansao").val()+'" class="delete"><i class="fa fa-trash"></i></a>'
                                    ] ).draw( false );
                                }
                            }
                        })
                        
                        
                    }
                })
                
            })
            $("body").on("click",".edit", function() {
                var maBanSao = $(this).attr("data-id");
                rowData  = $(this).parent().parent();
                row = table.row($(this).parent().parent()).data();
                
                $.ajax({url: "${pageContext.request.contextPath}/BanSaoSach/getBanSaoSach?maBanSao="+maBanSao,
                        type: "GET",
                        dataType: "json",
                        success: function(response) {
                            $("#m_mabansao").prop("readonly", true);
                            $("#m_idsach").val(response.maSach);
                            $("#m_mabansao").val(response.maBanSao);
                            $("#m_trangthai").val(response.trangthai);
                            document.getElementById("m_ngayxuatban").valueAsDate = new Date(response.ngayXuatBan)
                            $("#m_giatien").val(response.giaTien);
                            $(".m_active").addClass("active");
                        }
                })
            })
            $("body").on("click", ".delete", function() {
                var r = confirm("Bạn có chắc sẽ xoá dữ liệu này?");
                if (r == true) {
                    var mathanhvien = $(this).attr("data-id");
                    rowData  = $(this).parent().parent();
                    row = table.row($(this).parent().parent()).data();

                    $.ajax({url: "${pageContext.request.contextPath}/BanSaoSach/XoaBanSao?maBansao="+mathanhvien,
                            type: "POST",
                            dataType: "json",
                            success: function(response) {
                                if(response == '0') {
                                    $("#content_popup").html("Xoá thành công.");
                                    table.row(rowData).remove().draw();
                                } else {
                                    $("#content_popup").html("Bạn không thể xoá do đã phát sinh dữ liệu.");
                                }
                                $("#modalRegisterForm").modal("hide");
                                $("#frameModalBottom").modal('show');
                            }
                    })
                } 
                
            })
            
            $('#modalRegisterForm').on('hidden.bs.modal', function () {
                $("#m_mabansao").prop("readonly", false);
                row = null;
             })

        })
    </script> 
</body>
</html>
