<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý thư viện</title>
    </head>
    <style>
        #m_idsach_chosen {
            width :100% !important;
        }
    </style>
    <body>
        <!--Main Navigation-->
    <%@include file="../menu.jsp"%>
    <!--Main Navigation-->

    <main>
        
        <div class="container-fluid">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb cyan lighten-4">
                  <li class="breadcrumb-item"><a class="black-text" href="${pageContext.request.contextPath}">Trang chủ</a></li>
                  <li class="breadcrumb-item active">Danh sách phiếu mượn sách</li>
                </ol>
            </nav>
            <form class="row" id="form">
                <div class="col-md-10">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4 ">
                                <div class="md-form form-sm">
                                    <input type="text" id="txtsoPhieu" class="form-control form-control-sm" name="soPhieu">
                                    <label for="txtsoPhieu">Số phiếu</label></div>
                            </div>
                            <div class="col-md-4 ">
                                <div class="md-form form-sm">
                                    <input type="text" id="txtMaSach" class="form-control form-control-sm" name="maSach">
                                    <label for="txtMaSach">Mã sách</label></div>
                            </div>
                            <div class="col-md-4">
                                <div class="md-form form-sm">
                                    <input type="text" id="txtTenSach" class="form-control form-control-sm" name="tenSach">
                                    <label for="txtTenSach">Tên sách</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="md-form form-sm ">
                                    <label class="active" for="txtTuNgay">Từ ngày (ngày mượn)</label>
                                    <input type="date" id="txtTuNgay" class="form-control form-control-sm" name="tuNgay">
                                </div>

                            </div>
                            <div class="col-md-4 ">
                                <div class="md-form form-sm ">
                                    <label class="active" for="txtDenNgay">Đến ngày (ngày mượn)</label>
                                    <input type="date" id="txtDenNgay" class="form-control form-control-sm" name="denNgay">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="md-form form-sm">
                                    <input type="text" id="txtmathanhvien" class="form-control form-control-sm" name="maThanhVien">
                                    <label for="txtmathanhvien">Mã thành viên</label>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="md-form form-sm">
                                    <input type="text" id="txttenthanhvien" class="form-control form-control-sm" name="tenThanhVien">
                                    <label for="txttenthanhvien">Tên thành viên</label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="text-align: center">
                    <button type="submit" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-search" aria-hidden="true"></i> Tìm kiếm</button>
                </div>
            </form>
            <div class="divider-new">Danh sách phiếu mượn sách</div>
            <div class="row">
                <div class="col-auto">
                    <a class="btn btn-success btn-sm" href="" data-toggle="modal" data-target="#modalRegisterForm"><i class="fa fa-plus" aria-hidden="true"></i> Thêm bản sao</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table id="m_phieumuonsach" class="table table-sm table-bordered table-striped" style="text-align: center">
                        <thead>
                            <tr>
                                <th>
                                    Mã phiếu mượn
                                </th>
                                <th>
                                    Mã sách
                                </th>
                                <th>
                                    Tên sách
                                </th>
                                <th>
                                    Mã thành viên
                                </th>
                                <th>
                                    Tên thành viên
                                </th>
                                <th>
                                    Ngày mượn
                                </th>
                                <th>
                                    Ngày trả dự kiến 
                                </th>
                                <th>
                                    Ngày trả thực tế 
                                </th>
                                <th>Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${phieuMuons}" var="phieumuon">
                                <tr>
                                    <td>
                                        ${phieumuon.getSoPhieu()}
                                    </td>
                                    <td>
                                        <c:forEach items="${phieumuon.getBanSauSachs()}" var="bansao">
                                        ${bansao.getmaBanSao()} ;
                                        </c:forEach>
                                    </td>
                                    <td>
                                        <c:forEach items="${phieumuon.getBanSauSachs()}" var="bansao">
                                        ${bansao.sach.getTenSach()} ;
                                        </c:forEach>
                                    </td>
                                    <td>
                                         ${phieumuon.getThanhVien().getMaThanhVien()}
                                    </td>
                                    <td>
                                         ${phieumuon.getThanhVien().getHoTen()}
                                    </td>
                                    <td>
                                        ${formatter.format(phieumuon.getNgayMuon())}
                                    </td>
                                    <td>
                                        ${formatter.format(phieumuon.getNgayTra())}
                                    </td>
                                    <td>
                                       ${formatter.format(phieumuon.getNgayTraThucTe())}
                                    </td>
                                    <td style="text-align: center;letter-spacing: 5px;">
                                        <a style="color:green" href="" data-toggle="modal" data-target="#modalRegisterForm" class="edit" data-id="${phieumuon.getSoPhieu()}"><i class="fa fa-pencil"></i></a>
                                        <a style="color:red" data-id="${phieumuon.getSoPhieu()}" class="delete"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>

                            </c:forEach>

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header text-center">
                  <h4 class="modal-title w-100 font-weight-bold">Thông tin phiếu mượn sách</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body mx-8">
                    <div class="form-group row">
                            <label class="col-md-1">Sách</label>
                            <div class="col-md-11">
                                <select class="form-control chosen-select" multiple id="m_idsach">
                                    <c:forEach items="${sachs}" var="bangoc">
                                        <option value="${bangoc.getmaBanSao()}">${bangoc.getSach().getTenSach()}</option>
                                    </c:forEach>
                                </select>
                            </div>
                    </div>
                    <div class="form-group row">
                            <label class="col-md-1">TV</label>
                            <div class="col-md-11">
                                <select class="form-control" id="m_idthanhvien">
                                    <c:forEach items="${thanhviens}" var="thanhvien">
                                        <option value="${thanhvien.getMaThanhVien()}">${thanhvien.getHoTen()}</option>
                                    </c:forEach>
                                </select>
                            </div>
                    </div>
                  <div class="md-form mb-5">
                    <i class="fa fa-address-book prefix grey-text"></i>
                    <input type="text" id="m_sophieumuon" class="form-control">
                    <label data-error="wrong" class="m_active" data-success="right" for="m_sophieumuon">Số phiếu</label>
                  </div>
                  <div class="md-form mb-4">
                    <i class="fa fa-calendar prefix grey-text"></i>
                    <input type="date" id="m_ngaymuon" class="form-control" value="">
                    <label data-error="wrong" class="active" data-success="right" for="m_ngaymuon">Ngày mượn</label>
                  </div> 
                   <div class="md-form mb-4">
                    <i class="fa fa-calendar prefix grey-text"></i>
                    <input type="date" id="m_ngaytra" class="form-control" value="">
                    <label data-error="wrong" class="active" data-success="right" for="m_ngaytra">Ngày trả dự kiến </label>
                  </div> 
                    <div class="md-form mb-4">
                    <i class="fa fa-calendar prefix grey-text"></i>
                    <input type="date" id="m_ngaytrathucte" class="form-control" value="">
                    <label data-error="wrong" class="active" data-success="right" for="m_ngaytrathucte">Ngày trả thực tế</label>
                  </div>
                    <div class="md-form mb-5">
                    <i class="fa fa-address-book prefix grey-text"></i>
                    <input type="text" id="m_ghichu" class="form-control">
                    <label data-error="wrong" class="m_active" data-success="right" for="m_ghichu">Ghi chú</label>
                  </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                  <button id="luuthongtin" class="btn btn-deep-orange">Lưu</button>
                </div>
              </div>
            </div>
        </div>
        <div class="modal fade top" id="frameModalBottom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-frame modal-top" role="document">


              <div class="modal-content">
                <div class="modal-body">
                  <div class="row d-flex justify-content-center align-items-center">

                    <p class="pt-3 pr-2" id="content_popup"></p>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                  </div>
                </div>
              </div>
            </div>
        </div>             
    </main>
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/site.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/mdb.css" />"  rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css" />"  rel="stylesheet">
    <link href="<c:url value="/resources/css/chosen.css" />" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <script src="<c:url value="/resources/js/jquery.min.js" />" src=""></script>
    <script src="<c:url value="/resources/js/bootstrap.js" />" src=""></script>
    <script src="<c:url value="/resources/js/mdb.js" />"></script>
    <script src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
    <%@include file="../footer.jsp"%>
    <script>
        $.ajax()
        $(".chosen-select").chosen();
    </script>
    <script>
        function splitDate(string) {
            var t = string.split("-");
            return t[2]+"/"+t[1]+"/"+t[0];
        }
        $(function() {
            var table = $('#m_phieumuonsach').DataTable();
            var row;
            var rowData;
            $("#luuthongtin").click(function(){
                $.ajax({
                    url: "${pageContext.request.contextPath}/PhieuMuonSach/ThemPhieu",
                    type: "POST",
                    data: {
                        maSach: $("#m_idsach").val().join(','),
                        maThanhVien: $("#m_idthanhvien").val(),
                        soPhieuMuon: $("#m_sophieumuon").val(),
                        ngayMuon: $("#m_ngaymuon").val(),
                        ngayTra: $("#m_ngaytra").val(),
                        ngayTraThucTe: $("#m_ngaytrathucte").val(),
                        ghiChu: $("#m_ghichu").val()
                    },
                    success: function(response) {
                        $("#modalRegisterForm").modal("hide");
                        $("#content_popup").html(response);
                        $("#frameModalBottom").modal('show');
                        $.ajax({url: "${pageContext.request.contextPath}/PhieuMuonSach/getPhieuMuonSach?soPhieuMuon="+$("#m_sophieumuon").val(),
                            type: "GET",
                            dataType: "json",
                            success: function(response) {
                                if (row != null) {
                                    console.log("row",row,[row[0],response.maSach, 
                                        response.tenSach,response.maThanhVien,response.hoTen,
                                        response.ngayMuon,response.ngayTra, response.ngayTraThucTe
                                    ,row[8]])
                                    table.row(rowData).data([row[0],response.maSach, 
                                        response.tenSach,response.maThanhVien,response.hoTen,
                                        response.ngayMuon,response.ngayTra, response.ngayTraThucTe
                                    ,row[8]]).draw();
                                } else {
                                    table.row.add( [
                                        $("#m_sophieumuon").val()
                                        ,response.maSach, 
                                        response.tenSach,response.maThanhVien,response.hoTen,
                                        response.ngayMuon,response.ngayTra, response.ngayTraThucTe,
                                        '<a style="color:green" href="" data-toggle="modal" data-target="#modalRegisterForm" class="edit" data-id="'+ $("#m_sophieumuon").val()+'"><i class="fa fa-pencil"></i></a><a style="color:red" data-id="'+ $("#m_sophieumuon").val()+'" class="delete"><i class="fa fa-trash"></i></a>'
                                    ] ).draw( false );
                                }
                            }
                        })
                        
                        
                    }
                })
                
            })
            $("body").on("click",".edit", function() {
                var maBanSao = $(this).attr("data-id");
                rowData  = $(this).parent().parent();
                row = table.row($(this).parent().parent()).data();
                
                $.ajax({url: "${pageContext.request.contextPath}/PhieuMuonSach/getPhieuMuonSach?soPhieuMuon="+maBanSao,
                        type: "GET",
                        dataType: "json",
                        success: function(response) {
                            $("#m_sophieumuon").prop("readonly", true);
                            
                            
                            var values=response.maSach;
                            $.each(values.split(";"), function(i,e){
                                console.log("e,i", e,i);
                                $("#m_idsach option[value='" + e + "']").prop("selected", true);
                            });
                            $("#m_idsach").trigger("chosen:updated");
                            $("#m_sophieumuon").val(response.soPhieu);
                            $("#m_ghichu").val(response.ghiChu);
                            $("#m_idthanhvien").val(response.maThanhVien);
                            var ngayMuon = response.ngayMuon.split("/");
                            var ngaytra = response.ngayTra.split("/");
                            var ngaytratt = response.ngayTraThucTe.split("/");
                            document.getElementById("m_ngaymuon").valueAsDate = new Date(ngayMuon[2],ngayMuon[1]-1,ngayMuon[0]);
                            document.getElementById("m_ngaytra").valueAsDate = new Date(ngaytra[2],ngaytra[1]-1,ngaytra[0]);
                            document.getElementById("m_ngaytrathucte").valueAsDate = new Date(ngaytratt[2],ngaytratt[1]-1,ngaytratt[0]);
                            $(".m_active").addClass("active");
                        }
                })
            })
            $("body").on("click", ".delete", function() {
                var r = confirm("Bạn có chắc sẽ xoá dữ liệu này?");
                if (r == true) {
                    var mathanhvien = $(this).attr("data-id");
                    rowData  = $(this).parent().parent();
                    row = table.row($(this).parent().parent()).data();

                    $.ajax({url: "${pageContext.request.contextPath}/PhieuMuonSach/XoaPhieu?soPhieu="+mathanhvien,
                            type: "POST",
                            dataType: "json",
                            success: function(response) {
                                if(response == '0') {
                                    $("#content_popup").html("Xoá thành công.");
                                    table.row(rowData).remove().draw();
                                } else {
                                    $("#content_popup").html("Bạn không thể xoá do đã phát sinh dữ liệu.");
                                }
                                $("#modalRegisterForm").modal("hide");
                                $("#frameModalBottom").modal('show');
                            }
                    })
                } 
                
            })
            
            $('#modalRegisterForm').on('hidden.bs.modal', function () {
                $("#m_sophieumuon").prop("readonly", false);
                row = null;
             })

        })
    </script> 
</body>
</html>
