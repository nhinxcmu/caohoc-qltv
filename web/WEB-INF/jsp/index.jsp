<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý thư viện</title>
    </head>

    <body>
        <!--Main Navigation-->
    <%@include file="menu.jsp"%>
    <!--Main Navigation-->

    <main>
        <div class="container-fluid">
            
            <div class="row">
                <div class="col">
                    <form method="post" action="/caohoc-qltv/consine" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3 text-center">
                                <h2>VNPT SEARCH</h2>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <input class="form-control" type="text" name="term"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">TÌM KIỂM</button>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </main>

    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/site.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/mdb.css" />"  rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css" />"  rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <script src="<c:url value="/resources/js/jquery.min.js" />" src=""></script>
    <script src="<c:url value="/resources/js/bootstrap.js" />" src=""></script>
    <script src="<c:url value="/resources/js/mdb.js" />"></script>
    <link href="<c:url value="/resources/jqgrid/css/ui.jqgrid.css" />" rel="stylesheet"/>           
    <script src="<c:url value="/resources/jqgrid/js/i18n/grid.locale-en.js" />"></script>            
    <script src="<c:url value="/resources/jqgrid/js/jquery.jqGrid.src.js" />"></script>
</body>
</html>
