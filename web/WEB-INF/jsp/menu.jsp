<%@page contentType="text/html" pageEncoding="UTF-8"%>
<header>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark blue-gradient scrolling-navbar">
        <a class="navbar-brand" href="#"><strong>VNPT SEARCH</strong></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light m_nav-link" data-id="${requestScope['javax.servlet.forward.request_uri']}" href="${pageContext.request.contextPath}/">Trang chủ </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light m_nav-link" data-id="${requestScope['javax.servlet.forward.request_uri']}" href="${pageContext.request.contextPath}/invertedindex/">Tao index</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light m_nav-link" data-id="${requestScope['javax.servlet.forward.request_uri']}" href="${pageContext.request.contextPath}/indexed/">Kết quả index</a>
                </li>
            </ul>
            <ul class="navbar-nav nav-flex-icons">
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light"><i class="fa fa-facebook"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light"><i class="fa fa-twitter"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light"><i class="fa fa-instagram"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
