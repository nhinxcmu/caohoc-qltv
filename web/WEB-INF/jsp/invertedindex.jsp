<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>VNPT SEARCH</title>
        <style>
            .file-field input[type=file] {
                position: absolute;
                top: 0;
                right: 0;
                left: 0;
                bottom: 0;
                width: 100%;
                margin: 0;
                padding: 0;
                cursor: pointer;
                opacity: 0;
            }
            .file-field input.file-path {
                height: 33px;
            }
            .file-field {
                position: relative;
            }
        </style>    
    </head>

    <body>
        <!--Main Navigation-->
    <%@include file="menu.jsp"%>
    <!--Main Navigation-->

    <main>
        <div class="container-fluid">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb cyan lighten-4"> 
                    <li class="breadcrumb-item"><a class="black-text" href="${pageContext.request.contextPath}">Trang chủ</a></li>
                    <li class="breadcrumb-item active">Tạo inverted index</li>
                </ol>
            </nav>
            <div class="row">
                <div class="col">
                    <form enctype="multipart/form-data" class="md-form" method="post">
                        <div class="file-field col-md-6 offset-md-3">
                            <div class="btn btn-primary btn-sm float-left">
                                <span>Choose file</span>
                                <input type="file" name="doc1">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" accept=".txt" readonly type="text" placeholder="Upload document">
                            </div>
                        </div>
                        <div class="file-field col-md-6 offset-md-3">
                            <div class="btn btn-primary btn-sm float-left">
                                <span>Choose file</span>
                                <input type="file" name="doc2">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" accept=".txt" readonly type="text" placeholder="Upload document">
                            </div>
                        </div>
                        <div class="file-field col-md-6 offset-md-3">
                            <div class="btn btn-primary btn-sm float-left">
                                <span>Choose file</span>
                                <input type="file" name="doc3">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" accept=".txt" readonly type="text" placeholder="Upload document">
                            </div>
                        </div>
                        <div class="file-field col-md-6 offset-md-3">
                            <div class="btn btn-primary btn-sm float-left">
                                <span>Choose file</span>
                                <input type="file" name="doc4">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" accept=".txt" readonly type="text" placeholder="Upload document">
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <div class="col-auto">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">TẠO INDEX</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>

    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/site.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/mdb.css" />"  rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css" />"  rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <script src="<c:url value="/resources/js/jquery.min.js" />" src=""></script>
    <script src="<c:url value="/resources/js/bootstrap.js" />" src=""></script>
    <script src="<c:url value="/resources/js/mdb.js" />"></script>
    <link href="<c:url value="/resources/jqgrid/css/ui.jqgrid.css" />" rel="stylesheet"/>           
    <script src="<c:url value="/resources/jqgrid/js/i18n/grid.locale-en.js" />"></script>            
    <script src="<c:url value="/resources/jqgrid/js/jquery.jqGrid.src.js" />"></script>
    <script src="<c:url value="/resources/jqgrid/js/jquery.jqGrid.src.js" />"></script>
     <script src="<c:url value="/resources/js/compiled.0.min.js" />"></script>
    
</body>
</html>
