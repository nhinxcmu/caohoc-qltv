
$.extend($.jgrid.edit, {
    ajaxEditOptions: { contentType: "application/json" },
    mtype: 'PUT',
    width: 500,
    addCaption: 'Thêm mới',
    editCaption: 'Chỉnh sửa',
    reloadAfterSubmit:true,
    closeOnEscape: true,
    closeAfterAdd:true,
    closeAfterEdit:true,
    recreateForm:true,
    serializeEditData: function(data) {
        delete data.oper;
        delete data.id;
        return JSON.stringify(data);
    }
});

$.extend($.jgrid.del, {
    mtype: 'DELETE',
    serializeDelData: function() {
        return "";
    }
});